FROM php:7.1-apache

ARG UID
ARG GID
ARG T_APACHE_DOCUMENT_ROOT
ARG T_APACHE_RUN_USER
ARG T_APACHE_RUN_GROUP

ENV APACHE_DOCUMENT_ROOT ${T_APACHE_DOCUMENT_ROOT}
ENV APACHE_RUN_USER ${T_APACHE_RUN_USER}
ENV APACHE_RUN_GROUP ${T_APACHE_RUN_GROUP}

RUN usermod -u ${UID} ${APACHE_RUN_USER} && groupmod -g ${GID} ${APACHE_RUN_GROUP}

RUN apt-get update && apt-get install -y libmagickwand-dev ffmpeg mysql-client vim --no-install-recommends && pecl install imagick && docker-php-ext-enable imagick && pecl install xdebug && docker-php-ext-enable xdebug && docker-php-ext-install mysqli && docker-php-ext-install pdo pdo_mysql soap


RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite && service apache2 restart
