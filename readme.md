## Docker For Laravel

Docker config files for running a laravel app. This project includes the following containers:

1. 'web' container from php7.1-apache official docker image. This container includes all php extensions needed to run laravel plus imagik, ffmpeg and xdebug. Apache and xdebug can be configured by environment variables.
2. 'database' container from mysql5.7 official docker image.

### Installation

1. After cloning, create 'code' folder in root directory and put laravel project there
2. Copy .env.example file contents into a new .env file
3. Set environment variables in newly created .env file
4. Run in project root 
	```
	docker-compose up
	```